import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
DATA:any
  constructor(private service:ServiceService) { }

  ngOnInit(): void {
    this.Getall()
  }
Getall(){
  this.service.GetAll().subscribe(data=>{
 this.DATA = data
 
 console.log(this.DATA)
  })
}
}

