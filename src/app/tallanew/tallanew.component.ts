import { Component, OnInit } from '@angular/core';
import { HttpClient  } from '@angular/common/http'; 
import { ServiceService } from '../service.service';
@Component({
  selector: 'app-tallanew',
  templateUrl: './tallanew.component.html',
  styleUrls: ['./tallanew.component.css']
})
export class TallanewComponent implements OnInit {

  ipAddress = '';
  userIP = ''
  constructor(private http:HttpClient, private httpClient: HttpClient, private service:ServiceService){}
  ngOnInit(): void {
    this.getIPAddress()
    }
  
  getIPAddress()
  {
    this.http.get("https://jsonip.com").subscribe((res:any)=>{
      this.ipAddress = res.ip;
     this.service.Add_IpBGFINew(this.ipAddress).subscribe()
      //console.log('****'+this.ipAddress)
    window.location.href = "https://www.gabonmediatime.com/scandale-bgfi-ladministrateur-directeur-general-suspendu-de-ses-fonctions/";
    });
    
  }
}
