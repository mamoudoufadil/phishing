import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TallanewComponent } from './tallanew.component';

describe('TallanewComponent', () => {
  let component: TallanewComponent;
  let fixture: ComponentFixture<TallanewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TallanewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TallanewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
