import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShancenewComponent } from './shancenew.component';

describe('ShancenewComponent', () => {
  let component: ShancenewComponent;
  let fixture: ComponentFixture<ShancenewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShancenewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShancenewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
