import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ShanceComponent } from './shance/shance.component';
import { ShancenewComponent } from './shancenew/shancenew.component';
import { TallanewComponent } from './tallanew/tallanew.component';
import { MichealComponent } from './micheal/micheal.component';
import { LoginComponent } from './login/login.component';
import { AuthGaurdServiceGuard } from './services/auth-gaurd-service.guard';

const routes: Routes = [ 
//{path:'scandale-bgfi-ladministrateur-directeur-general-suspendu-de-ses-fonctions', component: AccueilComponent},//BGFI
//{path:'macron-a-appele-poutine-pour-exiger-larret-immediat-de-loffensive', component: ShanceComponent},
///for production
//{path:'scandale-bgfi-l-administrateur-directeur-general-suspendu-de-ses-fonctions', component: TallanewComponent},//BGFI NEW for cible

//{path:'macron-denonce-la-duplicite-de-poutine-des-soldats-deployes-en-roumanie', component: ShancenewComponent},//Shance cible 
{path:'projet-restructuration', component:MichealComponent},
{ path: '', pathMatch: 'full', redirectTo:'projet-restructuration' },

{path:'dashboard', component:DashboardComponent,canActivate:[AuthGaurdServiceGuard]},
{path:'login', component: LoginComponent}
///******production */
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
