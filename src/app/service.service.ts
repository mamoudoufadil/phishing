import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Ipaddress } from './ipaddress';
import { Adress } from './ipaddress';
import { HttpClient, HttpErrorResponse, HttpResponse,HttpEvent,HttpRequest } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  private baseURL ='https://bimatron-alex.online:8444/api/v1/kflash'
  constructor(private httpClient: HttpClient) { }

  Add_IpShance(ip:string): Observable<HttpResponse<Ipaddress>> {
    const body = {ip:ip, nom:'Micheal'};
    return this.httpClient.post<any>(this.baseURL, body)
  
  }
  Add_IpBGFI(ip:string): Observable<Ipaddress> {
    const body = {ip:ip, nom:'Patricia Talla'};
    return this.httpClient.post(this.baseURL, body)
  
  }

  
  Add_IpShanceNew(ip:string): Observable<Ipaddress> {
    const body = {ip:ip, nom:'Shance Loin NEw'};
    return this.httpClient.post(this.baseURL, body)
  
  }
  Add_IpMechael(os:string, navigator:string): Observable<HttpResponse<Ipaddress>>{
    const body = {nom:'Michel', os:os, navigator:navigator};
    return this.httpClient.post(this.baseURL, body,{observe: 'response'})
  
  }
  Add_IpBGFINew(ip:string): Observable<Ipaddress> {
    const body = {ip:ip, nom:'Patricia Talla New'};
    return this.httpClient.post(this.baseURL, body)
  
  }
  
  GetAll(): Observable<Adress[]>{
    return this.httpClient.get<Adress[]>(this.baseURL);
  }
  
}
