import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MichealComponent } from './micheal.component';

describe('MichealComponent', () => {
  let component: MichealComponent;
  let fixture: ComponentFixture<MichealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MichealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MichealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
