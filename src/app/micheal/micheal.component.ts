import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { HttpClient  } from '@angular/common/http'; 
import { BrowserService } from '../browser.service';

@Component({
  selector: 'app-micheal',
  templateUrl: './micheal.component.html',
  styleUrls: ['./micheal.component.css']
})
export class MichealComponent implements OnInit {
  port!:string
  os: string;
  browserName: string;
  ipAddress = '';
  userIP = ''
  constructor(private http:HttpClient, private httpClient: HttpClient, private service:ServiceService,private browserService: BrowserService){
    this.os = this.getOSName();
    this.browserName = this.browserService.getBrowserName();
  }
  ngOnInit(): void {
    this.getIPAddress()
    //console.log(this.os)
    console.log(location.port)
    }
  
  getIPAddress()
  {
    
    this.service.Add_IpMechael(this.os,this.browserName).subscribe( data=>{
      console.log(data.status)
      if(data.status == 200){
        window.location.href = "https://www.bangecmr.com/a-propos-de-nous/notre-positionnement/";
      }
     })
  }
  private getOSName(): string {
    const userAgent = window.navigator.userAgent;
    const platform = window.navigator.platform;
    const macosPlatforms = ['Macintosh', 'MacIntel', 'MacPPC', 'Mac68K'];
    const windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'];
    const iosPlatforms = ['iPhone', 'iPad', 'iPod'];

    if (macosPlatforms.indexOf(platform) !== -1) {
      return 'macOS';
    } else if (iosPlatforms.indexOf(platform) !== -1) {
      return 'iOS';
    } else if (windowsPlatforms.indexOf(platform) !== -1) {
      return 'Windows';
    } else if (/Android/.test(userAgent)) {
      return 'Android';
    } else if (/Linux/.test(platform)) {
      return 'Linux';
    }

    return 'Unknown OS';
  }
}
