export class Ipaddress {
    ip?:string
    nom?:string
    os?:string
    navigator?:string
    port?:string
    zonedDate?:string
    timestampZoned?:string
}

export class Adress {
    id?: number
    ip?:string
    nom?:string
    jsonip?: any
    dateLoc?:string
}