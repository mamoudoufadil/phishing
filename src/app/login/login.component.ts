import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthentificationService } from '../services/authentification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username:any
  password = ''
  invalidLogin = false
    constructor(private router : Router, private loginservice : AuthentificationService) { }
  
    ngOnInit(): void {
    }
  checkLogin(){
    if(this.loginservice.authenticate(this.username, this.password)){
      this.router.navigate(['/dashboard'])
      this.invalidLogin = false
   
    } else 
    this.invalidLogin = true
    this.username = ''
    this.password = ''
  }
  
  }
  