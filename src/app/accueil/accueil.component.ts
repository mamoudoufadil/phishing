import { Component, OnInit } from '@angular/core';
import { HttpClient  } from '@angular/common/http'; 
import { ServiceService } from '../service.service';
@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  ipAddress = '';
  userIP = ''
  constructor(private http:HttpClient, private httpClient: HttpClient, private service:ServiceService){}
  ngOnInit(): void {
    this.getIPAddress()
    }
  
  getIPAddress()
  {
    this.http.get("https://jsonip.com").subscribe((res:any)=>{
      this.ipAddress = res.ip;
     this.service.Add_IpBGFI(this.ipAddress).subscribe()
      //console.log('****'+this.ipAddress)
    window.location.href = "https://www.gabonmediatime.com/scandale-bgfi-ladministrateur-directeur-general-suspendu-de-ses-fonctions/";
    });
    
  }

  /*loadIp() {
    this.httpClient.get('https://jsonip.com').subscribe(
      (value:any) => {
        console.log(value);
        this.userIP = value.ip;
      },
      (error) => {
        console.log(error);
      }
    );
  }*/
}
