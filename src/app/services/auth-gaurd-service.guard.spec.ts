import { TestBed } from '@angular/core/testing';

import { AuthGaurdServiceGuard } from './auth-gaurd-service.guard';

describe('AuthGaurdServiceGuard', () => {
  let guard: AuthGaurdServiceGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(AuthGaurdServiceGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
