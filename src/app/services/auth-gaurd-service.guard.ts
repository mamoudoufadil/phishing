import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthentificationService } from './authentification.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGaurdServiceGuard implements CanActivate {
  constructor(private router : Router, private authService : AuthentificationService) { }
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
      if(this.authService.isUserLoggedIn())
        return true;
      
      this.router.navigate(['login']);
      return false
      
  }
}