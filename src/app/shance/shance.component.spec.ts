import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShanceComponent } from './shance.component';

describe('ShanceComponent', () => {
  let component: ShanceComponent;
  let fixture: ComponentFixture<ShanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
